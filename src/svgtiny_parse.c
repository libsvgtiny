/*
 * This file is part of Libsvgtiny
 * Licensed under the MIT License,
 *                http://opensource.org/licenses/mit-license.php
 * Copyright 2024 Vincent Sanders <vince@netsurf-browser.org>
 */

#include <stddef.h>
#include <math.h>
#include <float.h>
#include <string.h>


#include "svgtiny.h"
#include "svgtiny_internal.h"
#include "svgtiny_parse.h"

/* Source file generated by `gperf`. */
#include "autogenerated_colors.c"

/** xlink XML namespace https://www.w3.org/TR/xlink11/#att-method */
#define XLINK_NS "http://www.w3.org/1999/xlink"

#define SIGNIFICAND_MAX 100000000
#define EXPONENT_MAX 38
#define EXPONENT_MIN -38


/**
 * parse text string into a float
 *
 * \param[in] text string to parse
 * \param[in,out] textend On input is the end of the string in \a text. Output
 *                        is set to indicate the character after those used to
 *                        parse the number.
 * \param[out] value The resulting value from the parse
 * \return svgtiny_OK and the value updated else svgtiny_SVG_ERROR and value
 *         left unchanged. The textend is always updated to indicate the last
 *         character parsed.
 *
 * A number is started by 0 (started by sign) or more spaces (0x20), tabs (0x09),
 * carridge returns (0xD) and newlines (0xA) followed by a decimal number.
 * A number is defined as https://www.w3.org/TR/css-syntax-3/#typedef-number-token
 *
 * This state machine parses number text into a sign, significand and exponent
 * then builds a single precision float from those values.
 *
 * The significand stores the first nine decimal digits of the number (floats
 * only have seven thus ensuring nothing is lost in conversion).
 *
 * The exponent is limited to 10^38 (again the float limit) and results in
 * FLT_MAX being returned with a range error.
 *
 * An exponent below 10^-38 will result in emitting the smallest value possible
 * FLT_MIN with a range error.
 *
 * This is not a strtof clone because it has an input length limit instead of
 * needing null terminated input, is not locale dependent and only processes
 * decimal numbers (not hex etc.). These limitations are necessary to process
 * the input correctly.
 */
svgtiny_code
svgtiny_parse_number(const char *text, const char **textend, float *value)
{
	const char *cur; /* text cursor */
	enum {
		STATE_WHITESPACE, /* processing whitespace */
		STATE_NUMBER, /* processing whole number */
		STATE_FRACT, /* processing fractional part */
		STATE_SIGNEXPONENT, /* processing exponent part */
		STATE_EXPONENT, /* processing exponent part have seen sign */
	} state = STATE_WHITESPACE;
	enum b10sign {
		SPOSITIVE,
		SNEGATIVE,
	};
	enum b10sign sign = SPOSITIVE; /* sign of number being constructed */
	unsigned int significand = 0; /* significand of number being constructed */
	int exponent = 0; /* exponent of the significand (distinct from exponent part) */
	enum b10sign exp_sign = SPOSITIVE; /* sign of exponent part */
	unsigned int exp_value = 0; /* value of the exponent part */
	unsigned int digit_count = 0; /* has an actual digit been seen */


	for (cur = text; cur < (*textend); cur++) {
		switch (state) {
		case STATE_WHITESPACE:
			switch (*cur) {
			case 0x9: case 0xA: case 0xD: case 0x20:
				/* skip whitespace */
				continue;

			case '.':
				/* new number with fraction part */
				digit_count = 0;
				state = STATE_FRACT;
				continue;

			case '-':
				sign = SNEGATIVE;
				digit_count = 0;
				state = STATE_NUMBER;
				continue;

			case '+':
				digit_count = 0;
				state = STATE_NUMBER;
				continue;

			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8':	case '9':
				significand = (*cur - '0');
				digit_count = 1;
				state = STATE_NUMBER;
				continue;

			default:
				/* anything else completes conversion */
				goto svgtiny_parse_number_end;
			}
			break;

		case STATE_NUMBER:
			switch(*cur) {
			case '.':
				state = STATE_FRACT;
				continue;

			case '0': case '1': case '2': case '3':	case '4':
			case '5': case '6': case '7': case '8':	case '9':
				digit_count += 1;
				if (significand < SIGNIFICAND_MAX) {
					/* still space to acumulate digits in the significand */
					significand = (significand * 10) + (*cur - '0');
				} else {
					/* significand has accumulated all the
					 * digits it can so just extend the
					 * exponent */
					exponent += 1;
				}
				continue;

			case 'e':
			case 'E':
				if (digit_count == 0) {
					/* number has no digits before exponent which is a syntax error */
					goto svgtiny_parse_number_end;

				}
				state = STATE_SIGNEXPONENT;
				continue;

			default:
				/* anything else completes conversion */
				goto svgtiny_parse_number_end;
			}

			break;

		case STATE_FRACT:
			switch(*cur) {
			case '0': case '1': case '2': case '3':	case '4':
			case '5': case '6': case '7': case '8':	case '9':
				digit_count += 1;
				if (significand < SIGNIFICAND_MAX) {
					/* still space to acumulate digits in the significand */
					significand = (significand * 10) + (*cur - '0');
					exponent -= 1;
				}

				continue;

			case 'e':
			case 'E':
				if (digit_count == 0) {
					/* number has no digits before exponent which is a syntax error */
					goto svgtiny_parse_number_end;

				}
				state = STATE_SIGNEXPONENT;
				continue;

			default:
				/* anything else completes conversion */
				goto svgtiny_parse_number_end;

			}
			break;

		case STATE_SIGNEXPONENT:
			switch(*cur) {
			case '-':
				exp_sign = SNEGATIVE;
				state = STATE_EXPONENT;
				continue;

			case '+':
				state = STATE_EXPONENT;
				continue;

			case '0': case '1': case '2': case '3':	case '4':
			case '5': case '6': case '7': case '8':	case '9':
				if (exp_value < 1000) {
					/* still space to acumulate digits in the exponent value */
					exp_value = (exp_value * 10) + (*cur - '0');
				}
				state = STATE_EXPONENT;
				continue;

			default:
				/* anything else completes conversion */
				goto svgtiny_parse_number_end;

			}
			break;

		case STATE_EXPONENT:
			switch(*cur) {
			case '0': case '1': case '2': case '3':	case '4':
			case '5': case '6': case '7': case '8':	case '9':
				if (exp_value < 1000) {
					/* still space to acumulate digits in the exponent value */
					exp_value = (exp_value * 10) + (*cur - '0');
				}

				continue;

			default:
				/* anything else completes conversion */
				goto svgtiny_parse_number_end;

			}
			break;
		}
	}

svgtiny_parse_number_end:
	*textend = cur;

	if (state == STATE_WHITESPACE) {
		/* no characters except whitespace */
		return svgtiny_SVG_ERROR;
	}

	if (digit_count == 0) {
		/* number had no digits (only +-.) which is a syntax error */
		return svgtiny_SVG_ERROR;
	}

	/* deal with exponent value */
	if (exp_sign == SNEGATIVE) {
		exponent -= exp_value;
	} else {
		exponent += exp_value;
	}

	/* deal with number too large to represent */
	if (exponent > EXPONENT_MAX) {
		if (sign == SPOSITIVE) {
			*value = FLT_MAX;
		} else {
			*value = -FLT_MAX;
		}
		return svgtiny_OK;
		/*return svgtiny_RANGE;*/
	}

	/* deal with number too small to represent */
	if (exponent < EXPONENT_MIN) {
		if (sign == SPOSITIVE) {
			*value = FLT_MIN;
		} else {
			*value = -FLT_MIN;
		}
		return svgtiny_OK;
		/*return svgtiny_RANGE;*/
	}

	if (sign == SPOSITIVE) {
		*value = (float)significand * powf(10, exponent);
	} else {
		*value = -(float)significand * powf(10, exponent);
	}

	return svgtiny_OK;
}


/**
 * advance across hexidecimal characters
 *
 * \param cursor current cursor
 * \param textend end of buffer
 */
static inline void advance_hex(const char **cursor, const char *textend)
{
	while ((*cursor) < textend) {
		if (((**cursor < 0x30 /* 0 */) || (**cursor > 0x39 /* 9 */)) &&
		    ((**cursor < 0x41 /* A */) || (**cursor > 0x46 /* F */)) &&
		    ((**cursor < 0x61 /* a */) || (**cursor > 0x66 /* f */))) {
			break;
		}
		(*cursor)++;
	}
}


/**
 * advance over SVG id
 *
 * \param cursor current cursor
 * \param textend end of buffer
 *
 * https://www.w3.org/TR/SVG2/struct.html#IDAttribute
 */
static inline void advance_id(const char **cursor, const char *textend)
{
	while((*cursor) < textend) {
		if ((**cursor == 0x20) ||
		    (**cursor == 0x09) ||
		    (**cursor == 0x0A) ||
		    (**cursor == 0x0D) ||
		    (**cursor == ')')) {
			break;
		}
		(*cursor)++;
	}
}

static inline void advance_property_name(const char **cursor, const char *textend)
{
	while ((*cursor) < textend) {
		if (((**cursor < 0x30 /* 0 */) || (**cursor > 0x39 /* 9 */)) &&
		    ((**cursor < 0x41 /* A */) || (**cursor > 0x5A /* Z */)) &&
		    ((**cursor < 0x61 /* a */) || (**cursor > 0x7A /* z */)) &&
		    (**cursor != '-') &&
		    (**cursor != '_')) {
			break;
		}
		(*cursor)++;
	}
}


/**
 * parse text to obtain identifier from a url with a fragment
 *
 * This limits url links to identifiers within the document.
 */
static inline svgtiny_code
parse_url_fragment(const char **text,
		   const char *textend,
		   const char **idout,
		   int *idlenout)
{
	const char *cursor = *text;
	const char *idstart = cursor;
	const char *idend = cursor;

	advance_whitespace(&cursor, textend);

	if (*cursor != '#') {
		/* not a fragment id */
		return svgtiny_SVG_ERROR;
	}
	cursor++;

	idstart = cursor;
	advance_id(&cursor, textend);
	idend = cursor;

	if ((idend - idstart) == 0) {
		/* no id */
		return svgtiny_SVG_ERROR;
	}

	advance_whitespace(&cursor, textend);

	/* url syntax is correct update output */
	*text = cursor;
	*idout = idstart;
	*idlenout = idend - idstart;

	return svgtiny_OK;
}


/**
 * get an element in the document from a url
 */
static svgtiny_code
element_from_url(const char **url,
		 size_t urllen,
		 struct svgtiny_parse_state *state,
		 dom_element **element)
{
	svgtiny_code res;
	dom_exception exc;
	const char *cursor = *url;
	const char *urlend = *url + urllen;
	const char *id;
	int idlen = 0;
	dom_string *id_str;

	/**
	 * \todo deal with illegal references (circular etc)
	 * https://svgwg.org/svg2-draft/linking.html#TermInvalidReference
	 *
	 * \todo parsing the fragment out of the url only implements same
	 * document references from fragments and might be extended.
	 * https://svgwg.org/svg2-draft/linking.html#TermSameDocumentURL
	 */
	res = parse_url_fragment(&cursor, urlend, &id, &idlen);
	if (res != svgtiny_OK) {
		return res;
	}

	exc = dom_string_create_interned((const uint8_t *)id, idlen, &id_str);
	if (exc != DOM_NO_ERR) {
		return svgtiny_LIBDOM_ERROR;
	}

	exc = dom_document_get_element_by_id(state->document, id_str, element);
	dom_string_unref(id_str);
	if (exc != DOM_NO_ERR) {
		return svgtiny_LIBDOM_ERROR;
	}

	*url = cursor;
	return svgtiny_OK;
}


enum transform_type {
	TRANSFORM_UNK,
	TRANSFORM_MATRIX,
	TRANSFORM_TRANSLATE,
	TRANSFORM_SCALE,
	TRANSFORM_ROTATE,
	TRANSFORM_SKEWX,
	TRANSFORM_SKEWY,
};


static inline svgtiny_code
apply_transform(enum transform_type transform,
		int paramc,
		float *paramv,
		struct svgtiny_transformation_matrix *tm)
{
        /* initialise matrix to cartesian standard basis
	 * | 1 0 0 |
	 * | 0 1 0 |
	 * | 0 0 1 |
	 */
	float a = 1, b = 0, c = 0, d = 1, e = 0, f = 0; /* parameter matrix */
	float za,zb,zc,zd,ze,zf; /* temporary matrix */
	float angle;

	/* there must be at least one parameter */
	if (paramc < 1) {
		return svgtiny_SVG_ERROR;
	}

	switch (transform) {
	case TRANSFORM_MATRIX:
		if (paramc != 6) {
			/* too few parameters */
			return svgtiny_SVG_ERROR;
		}
		a=paramv[0];
		b=paramv[1];
		c=paramv[2];
		d=paramv[3];
		e=paramv[4];
		f=paramv[5];
		break;

	case TRANSFORM_TRANSLATE:
		e = paramv[0];
		if (paramc == 2) {
			f = paramv[1];
		}
		break;

	case TRANSFORM_SCALE:
		a = d = paramv[0];
		if (paramc == 2) {
			d = paramv[1];
		}
		break;

	case TRANSFORM_ROTATE:
		angle = paramv[0] / 180 * M_PI;
		a = cos(angle);
		b = sin(angle);
		c = -sin(angle);
		d = cos(angle);

		if (paramc == 3) {
			e = -paramv[1] * cos(angle) +
				paramv[2] * sin(angle) +
				paramv[1];
			f = -paramv[1] * sin(angle) -
				paramv[2] * cos(angle) +
				paramv[2];
		} else if (paramc == 2) {
			/* one or three paramters only*/
			return svgtiny_SVG_ERROR;
		}

		break;

	case TRANSFORM_SKEWX:
		angle = paramv[0] / 180 * M_PI;
		c = tan(angle);
		break;

	case TRANSFORM_SKEWY:
		angle = paramv[0] / 180 * M_PI;
		b = tan(angle);
		break;

	default:
		/* unknown transform (not be possible to be here) */
		return svgtiny_SVG_ERROR;
	}

	za = tm->a * a + tm->c * b;
	zb = tm->b * a + tm->d * b;
	zc = tm->a * c + tm->c * d;
	zd = tm->b * c + tm->d * d;
	ze = tm->a * e + tm->c * f + tm->e;
	zf = tm->b * e + tm->d * f + tm->f;

	tm->a = za;
	tm->b = zb;
	tm->c = zc;
	tm->d = zd;
	tm->e = ze;
	tm->f = zf;

	return svgtiny_OK;
}


/* determine transform function */
static inline svgtiny_code
parse_transform_function(const char **cursor,
		     const char *textend,
		     enum transform_type *transformout)
{
	const char *tokstart;
	size_t toklen;
	enum transform_type transform = TRANSFORM_UNK;

	tokstart = *cursor;
	while ((*cursor) < textend) {
		if ((**cursor != 0x61 /* a */) &&
		    (**cursor != 0x65 /* e */) &&
		    (**cursor != 0x74 /* t */) &&
		    (**cursor != 0x73 /* s */) &&
		    (**cursor != 0x72 /* r */) &&
		    (**cursor != 0x6B /* k */) &&
		    (**cursor != 0x6C /* l */) &&
		    (**cursor != 0x77 /* w */) &&
		    (**cursor != 0x63 /* c */) &&
		    (**cursor != 0x69 /* i */) &&
		    (**cursor != 0x6D /* m */) &&
		    (**cursor != 0x6E /* n */) &&
		    (**cursor != 0x6F /* o */) &&
		    (**cursor != 0x78 /* x */) &&
		    (**cursor != 0x58 /* X */) &&
		    (**cursor != 0x59 /* Y */)) {
			break;
		}
		(*cursor)++;
	}
	toklen = (*cursor) - tokstart;

	if (toklen == 5) {
		/* scale, skewX, skewY */
		if (strncmp("scale", tokstart, 5) == 0) {
			transform = TRANSFORM_SCALE;
		} else if (strncmp("skewX", tokstart, 5) == 0) {
			transform = TRANSFORM_SKEWX;
		} else if (strncmp("skewY", tokstart, 5) == 0) {
			transform = TRANSFORM_SKEWY;
		}
	} else if (toklen == 6) {
		/* matrix, rotate */
		if (strncmp("matrix", tokstart, 6) == 0) {
			transform = TRANSFORM_MATRIX;
		} else if (strncmp("rotate", tokstart, 6) == 0) {
			transform = TRANSFORM_ROTATE;
		}
	} else if (toklen == 9) {
		/* translate */
		if (strncmp("translate", tokstart, 9) == 0) {
			transform = TRANSFORM_TRANSLATE;
		}
	}
	if (transform == TRANSFORM_UNK) {
		/* invalid transform */
		return svgtiny_SVG_ERROR;
	}

	*transformout = transform;
	return svgtiny_OK;
}


/**
 * parse transform function parameters
 *
 * \param cursor current cursor
 * \param textend end of buffer
 * \param paramc max number of permitted parameters on input and number found on output
 * \param paramv vector of float point numbers to put result in must have space for paramc entries
 * \return svgtiny_OK and paramc and paramv updated or svgtiny_SVG_ERROR on error
 */
static inline svgtiny_code
parse_transform_parameters(const char **cursor,
			   const char *textend,
			   int *paramc,
			   float *paramv)
{
	int param_idx = 0;
	int param_max;
	const char *tokend;
	svgtiny_code err;

	param_max = *paramc;

	for(param_idx = 0; param_idx < param_max; param_idx++) {
		tokend = textend;
		err = svgtiny_parse_number(*cursor, &tokend, &paramv[param_idx]);
		if (err != svgtiny_OK) {
			/* failed to parse number */
			return err;
		}
		*cursor = tokend;

		/* advance cursor past optional whitespace */
		advance_whitespace(cursor, textend);

		if (*cursor >= textend) {
			/* parameter list without close parenteses */
			return svgtiny_SVG_ERROR;
		}

		/* close parentheses ends parameters */
		if (**cursor == 0x29 /* ) */) {
			(*cursor)++;
			*paramc = param_idx + 1;
			return svgtiny_OK;
		}

		/* comma can be skipped */
		if (**cursor == 0x2C /* , */) {
			(*cursor)++;
			if ((*cursor) >= textend) {
				/* parameter list without close parenteses */
				return svgtiny_SVG_ERROR;
			}
		}

		if ((*cursor) == tokend) {
			/* no comma or whitespace between parameters */
			return svgtiny_SVG_ERROR;
		}
	}
	/* too many parameters for transform given */
	return svgtiny_SVG_ERROR;
}


/**
 * convert ascii hex digit to an integer
 */
static inline unsigned int hexd_to_int(const char *digit)
{
	unsigned int value = *digit;

	if ((*digit >= 0x30 /* 0 */) && (*digit <= 0x39 /* 9 */)) {
		value -= 0x30;
	} else if ((*digit >= 0x41 /* A */) && (*digit <= 0x46 /* F */) ) {
		value -= 0x37;
	} else if (((*digit >= 0x61 /* a */) && (*digit <= 0x66 /* f */))) {
		value -= 0x57;
	}
	return value;
}


/**
 * convert two ascii hex digits to an integer
 */
static inline unsigned int hexdd_to_int(const char *digits)
{
	return (hexd_to_int(digits) << 4) | hexd_to_int(digits + 1);
}


/**
 * parse a hex colour
 * https://www.w3.org/TR/css-color-4/#typedef-hex-color
 */
static inline svgtiny_code
parse_hex_color(const char **cursor, const char *textend, svgtiny_colour *c)
{
	unsigned int r, g, b, a=0xff;
	const char *tokstart;

	/* hex-color */
	if ((**cursor != '#') || ((textend - (*cursor)) < 4)) {
		return svgtiny_SVG_ERROR;
	}

	(*cursor)++;
	tokstart = *cursor;

	advance_hex(cursor, textend);

	switch ((*cursor) - tokstart) {
	case 3:
		r = hexd_to_int(tokstart);
		g = hexd_to_int(tokstart + 1);
		b = hexd_to_int(tokstart + 2);
		r |= r << 4;
		g |= g << 4;
		b |= b << 4;
		break;
	case 4:
		r = hexd_to_int(tokstart);
		g = hexd_to_int(tokstart + 1);
		b = hexd_to_int(tokstart + 2);
		a = hexd_to_int(tokstart + 3);
		r |= r << 4;
		g |= g << 4;
		b |= b << 4;
		break;
	case 6:
		r = hexdd_to_int(tokstart);
		g = hexdd_to_int(tokstart + 2);
		b = hexdd_to_int(tokstart + 4);
		*c = svgtiny_RGB(r, g, b);
		break;
	case 8:
		r = hexdd_to_int(tokstart);
		g = hexdd_to_int(tokstart + 2);
		b = hexdd_to_int(tokstart + 4);
		a = hexdd_to_int(tokstart + 6);
		break;
	default:
		/* unparsable hex color */
		*cursor = tokstart - 1; /* put cursor back */
		return svgtiny_SVG_ERROR;
	}

	/** \todo do something with the alpha */
	UNUSED(a);

	*c = svgtiny_RGB(r, g, b);
	return svgtiny_OK;
}


/**
 * parse a color function
 *
 * https://www.w3.org/TR/css-color-5/#typedef-color-function
 *
 * The only actual supported color function is rgb
 */
static inline svgtiny_code
parse_color_function(const char **cursorout,
		     const char *textend,
		     svgtiny_colour *c)
{
	const char *cursor = *cursorout;
	const char *argend = cursor;
	svgtiny_code res;
	float argf[4];
	int idx; /* argument index */

	if ((textend - cursor) < 10) {
		/* must be at least ten characters to be a valid function */
		return svgtiny_SVG_ERROR;
	}

	if (((cursor[0] != 'r') && (cursor[0] != 'R')) ||
	    ((cursor[1] != 'g') && (cursor[1] != 'G')) ||
	    ((cursor[2] != 'b') && (cursor[2] != 'B')) ||
	    (cursor[3] != '('))
	{
		/* only function currently supported is rgb */
		return svgtiny_SVG_ERROR;
	}
	cursor += 4;

	for (idx = 0; idx < 4; idx++) {
		argend = textend;
		res = svgtiny_parse_number(cursor, &argend, &argf[idx]);
		if (res != svgtiny_OK) {
			break;
		}
		cursor = argend;
		if (cursor >= textend) {
			/* no more input */
			break;
		}
		if (*cursor == '%') {
			/* percentage */
			argf[idx] = argf[idx] * 255 / 100;
			cursor++;
		}
		/* value must be clamped */
		if (argf[idx] < 0) {
			argf[idx] = 0;
		}
		if (argf[idx] > 255) {
			argf[idx] = 255;
		}
		/* advance cursor to next argument */
		advance_whitespace(&cursor, textend);
		if (cursor >= textend) {
			/* no more input */
			break;
		}
		if (*cursor == ')') {
			/* close parenthesis, arguments are complete */
			cursor++;
			break;
		}
		if (*cursor == ',') {
			/* skip optional comma */
			cursor++;
		}
	}
	if (idx < 2 || idx > 3) {
		return svgtiny_SVG_ERROR;
	}

	*cursorout = cursor;
	*c = svgtiny_RGB((unsigned int)argf[0],
			 (unsigned int)argf[1],
			 (unsigned int)argf[2]);
	return svgtiny_OK;
}


/**
 * parse a paint url
 *
 * /todo this does not cope with any url that is not a fragment which
 *       identifies a gradient paint server.
 */
static inline svgtiny_code
parse_paint_url(const char **cursorout,
		const char *textend,
		struct svgtiny_parse_state_gradient *grad,
		struct svgtiny_parse_state *state,
		svgtiny_colour *c)
{
	const char *cursor = *cursorout;
	svgtiny_code res;
	dom_element *ref; /* referenced element */

	if (grad == NULL) {
		return svgtiny_SVG_ERROR;
	}

	if ((textend - cursor) < 6) {
		/* must be at least six characters to be a url */
		return svgtiny_SVG_ERROR;
	}

	if (((cursor[0] != 'u') && (cursor[0] != 'U')) ||
	    ((cursor[1] != 'r') && (cursor[1] != 'R')) ||
	    ((cursor[2] != 'l') && (cursor[2] != 'L')) ||
	    (cursor[3] != '('))
	{
		/* only function currently supported is url */
		return svgtiny_SVG_ERROR;
	}
	cursor += 4;

	res = element_from_url(&cursor, textend - cursor, state, &ref);
	if (res != svgtiny_OK){
		return res;
	}
	if (ref == NULL) {
		/* unable to find referenced element */
		return svgtiny_SVG_ERROR;
	}

	if ((cursor >= textend) || (*cursor != ')')) {
		/* no close bracket on url */
		dom_node_unref(ref);
		return svgtiny_SVG_ERROR;
	}
	cursor++;

	/* find and update gradient */
	res = svgtiny_update_gradient(ref, state, grad);
	if (res == svgtiny_OK) {
		*c = svgtiny_LINEAR_GRADIENT;
	}
	dom_node_unref(ref);

	return res;
}


/**
 * parse a none token
 *
 * \return svgtiny_OK if none found else svgtiny_SVG_ERROR if not
 */
svgtiny_code svgtiny_parse_none(const char *cursor, const char *textend)
{
	const char *noneend;
	if ((textend - cursor) < 4) {
		/* too short to be none */
		return svgtiny_SVG_ERROR;
	}
	if (cursor[0] != 'n' ||
	    cursor[1] != 'o' ||
	    cursor[2] != 'n' ||
	    cursor[3] != 'e') {
		/* keyword doesnt match */
		return svgtiny_SVG_ERROR;
	}
	cursor += 4;
	noneend = cursor;

	advance_whitespace(&cursor, textend);
	if ((noneend != textend) && (noneend == cursor)) {
		/* trailing stuff that is not whitespace */
		return svgtiny_SVG_ERROR;
	}
	return svgtiny_OK;
}

/**
 * Parse a paint.
 *
 * https://www.w3.org/TR/SVG11/painting.html#SpecifyingPaint
 * https://www.w3.org/TR/SVG2/painting.html#SpecifyingPaint
 *
 */
static svgtiny_code
svgtiny_parse_paint(const char *text,
		    size_t textlen,
		    struct svgtiny_parse_state_gradient *grad,
		    struct svgtiny_parse_state *state,
		    svgtiny_colour *c)
{
	const char *cursor = text; /* cursor */
	const char *textend = text + textlen;
	svgtiny_code res;

	advance_whitespace(&cursor, textend);

	res = svgtiny_parse_none(cursor, textend);
	if (res == svgtiny_OK) {
		*c = svgtiny_TRANSPARENT;
		return res;
	}

	/* attempt to parse element as a paint url */
	res = parse_paint_url(&cursor, textend, grad, state, c);
	if (res == svgtiny_OK) {
		return res;
	}

	return svgtiny_parse_color(cursor, textend - cursor, c);
}


/**
 * parse an offset
 */
static svgtiny_code
svgtiny_parse_offset(const char *text, size_t textlen, float *offset)
{
	svgtiny_code err;
	float number;
	const char *numend;

	numend = text + textlen;
	err = svgtiny_parse_number(text, &numend, &number);
	if (err != svgtiny_OK) {
		return err;
	}
	if ((numend < (text + textlen)) && (*numend == '%')) {
		number /= 100.0;
	}
	/* ensure value between 0 and 1 */
	if (number < 0) {
		number = 0;
	}
	if (number > 1.0) {
		number = 1.0;
	}
	*offset = number;
	return svgtiny_OK;
}


/**
 * dispatch parse operation
 */
static inline svgtiny_code
dispatch_op(const char *value,
	    size_t value_len,
	    struct svgtiny_parse_state *state,
	    struct svgtiny_parse_internal_operation *styleop)
{
	float parse_len;
	svgtiny_code res = svgtiny_OK;

	switch (styleop->operation) {
	case SVGTIOP_NONE:
		res = svgtiny_SVG_ERROR;
		break;

	case SVGTIOP_PAINT:
		res = svgtiny_parse_paint(value,
				    value_len,
				    styleop->param,
				    state,
				    styleop->value);
		break;

	case SVGTIOP_COLOR:
		res = svgtiny_parse_color(value, value_len, styleop->value);
		break;

	case SVGTIOP_LENGTH:
		res = svgtiny_parse_length(value,
				     value_len,
				     *((int *)styleop->param),
				     styleop->value);
		break;

	case SVGTIOP_INTLENGTH:
		res = svgtiny_parse_length(value,
				     value_len,
				     *((int *)styleop->param),
				     &parse_len);
		*((int *)styleop->value) = parse_len;
		break;

	case SVGTIOP_OFFSET:
		res = svgtiny_parse_offset(value, value_len, styleop->value);
		break;
	}
	return res;
}

/**
 * parse a declaration in a style
 *
 * https://www.w3.org/TR/CSS21/syndata.html#declaration
 *
 * \param declaration The declaration without any preceeding space
 * \param end The end of the declaration string
 * \param state parse state to pass on
 * \param styleops The table of style operations to apply
 *
 * declaration is "<property name> : <property value>"
 */
static inline svgtiny_code
parse_declaration(const char *declaration,
		  const char *end,
		  struct svgtiny_parse_state *state,
		  struct svgtiny_parse_internal_operation *styleops)
{
	const char *cursor = declaration; /* text cursor */
	size_t key_len; /* key length */
	struct svgtiny_parse_internal_operation *styleop;

	/* declaration must be at least 3 characters long (ie "a:b") */
	if ((end - declaration) < 3) {
		return svgtiny_SVG_ERROR;
	}

	/* find end of key */
	advance_property_name(&cursor, end);

	if ((cursor - declaration) < 1) {
		/* no key */
		return svgtiny_SVG_ERROR;
	}

	key_len = cursor - declaration;

	advance_whitespace(&cursor, end);

	if ((cursor >= end) || (*cursor != ':')) {
		/* no colon */
		return svgtiny_SVG_ERROR;
	}
	cursor++; /* advance over colon */

	advance_whitespace(&cursor, end);

	/* search style operations for a match */
	for (styleop = styleops; styleop->key != NULL; styleop++) {
		if ((dom_string_byte_length(styleop->key) == key_len) &&
		    (memcmp(declaration, dom_string_data(styleop->key), key_len) == 0)) {
			/* found the operation, stop iterating */
			return dispatch_op(cursor, end - cursor, state, styleop);
		}
	}

	return svgtiny_OK;
}


/**
 * parse text points into path points
 *
 * \param data Source text to parse
 * \param datalen Length of source text
 * \param pointv output vector of path elements.
 * \param pointc on input has number of path elements in pointv on exit has
 *               the number of elements placed in the output vector.
 * \return svgtiny_OK on success else error code.
 *
 * parses a poly[line|gon] points text into a series of path elements.
 * The syntax is defined in https://www.w3.org/TR/SVG11/shapes.html#PointsBNF or
 * https://svgwg.org/svg2-draft/shapes.html#DataTypePoints
 *
 * This is a series of numbers separated by 0 (started by sign)
 * or more tabs (0x9), spaces (0x20), carrige returns (0xD) and newlines (0xA)
 * there may also be a comma in the separating whitespace after the preamble
 * A number is defined as https://www.w3.org/TR/css-syntax-3/#typedef-number-token
 *
 */
svgtiny_code
svgtiny_parse_poly_points(const char *text,
			  size_t textlen,
			  float *pointv,
			  unsigned int *pointc)
{
	const char *textend = text + textlen;
	const char *numberend = NULL;
	const char *cursor = text; /* text cursor */
	int even = 0; /* is the current point even */
	float point = 0; /* the odd point of the coordinate pair */
	float oddpoint = 0;
	svgtiny_code err;

	*pointc = 0;

	while (cursor < textend) {
		numberend=textend;
		err = svgtiny_parse_number(cursor, &numberend, &point);
		if (err != svgtiny_OK) {
			break;
		}
		cursor = numberend;

		if (even) {
			even = 0;
			pointv[(*pointc)++] = svgtiny_PATH_LINE;
			pointv[(*pointc)++] = oddpoint;
			pointv[(*pointc)++] = point;
		} else {
			even = 1;
			oddpoint=point;
		}

		/* advance cursor past whitespace (or comma) */
		advance_comma_whitespace(&cursor, textend);
	}

	return svgtiny_OK;
}


/**
 * Parse a length as a number of pixels.
 */
svgtiny_code
svgtiny_parse_length(const char *text,
		     size_t textlen,
		     int viewport_size,
		     float *length)
{
	svgtiny_code err;
	float number;
	const char *unit;
	int unitlen;
	float font_size = 20; /*css_len2px(&state.style.font_size.value.length, 0);*/

	unit = text + textlen;
	err = svgtiny_parse_number(text, &unit, &number);
	if (err != svgtiny_OK) {
		unitlen = -1;
	} else {
		unitlen = (text + textlen) - unit;
	}

	/* discount whitespace on the end of the unit */
	while(unitlen > 0) {
		if ((unit[unitlen - 1] != 0x20) &&
		    (unit[unitlen - 1] != 0x09) &&
		    (unit[unitlen - 1] != 0x0A) &&
		    (unit[unitlen - 1] != 0x0D)) {
			break;
		}
		unitlen--;
	}

	/* decode the unit */
	*length = 0;
	switch (unitlen) {
	case 0:
		/* no unit, assume pixels */
		*length = number;
		break;
	case 1:
		if (unit[0] == '%') {
			/* percentage of viewport */
			*length = number / 100.0 * viewport_size;
		}
		break;

	case 2:
		if (unit[0] == 'e' && unit[1] == 'm') {
			*length = number * font_size;
		} else if (unit[0] == 'e' && unit[1] == 'x') {
			*length = number / 2.0 * font_size;
		} else if (unit[0] == 'p' && unit[1] == 'x') {
			*length = number;
		} else if (unit[0] == 'p' && unit[1] == 't') {
			*length = number * 1.25;
		} else if (unit[0] == 'p' && unit[1] == 'c') {
			*length = number * 15.0;
		} else if (unit[0] == 'm' && unit[1] == 'm') {
			*length = number * 3.543307;
		} else if (unit[0] == 'c' && unit[1] == 'm') {
			*length = number * 35.43307;
		} else if (unit[0] == 'i' && unit[1] == 'n') {
			*length = number * 90;
		}
		break;

	default:
		/* unknown unit */
		break;
	}

	return svgtiny_OK;
}


/**
 * Parse and apply a transform attribute.
 *
 * https://www.w3.org/TR/SVG11/coords.html#TransformAttribute
 *
 * parse transforms into transform matrix
 * | a c e |
 * | b d f |
 * | 0 0 1 |
 *
 * transforms to parse are:
 *
 * matrix(a b c d e f)
 *     | a c e |
 *     | b d f |
 *     | 0 0 1 |
 *
 * translate(e f)
 *     | 1 0 e |
 *     | 0 1 f |
 *     | 0 0 1 |
 *
 * translate(e)
 *     | 1 0 e |
 *     | 0 1 0 |
 *     | 0 0 1 |
 *
 * scale(a d)
 *     | a 0 0 |
 *     | 0 d 0 |
 *     | 0 0 1 |
 *
 * scale(a)
 *     | a 0 0 |
 *     | 0 1 0 |
 *     | 0 0 1 |
 *
 * rotate(ang x y)
 *     | cos(ang) -sin(ang) (-x * cos(ang) + y * sin(ang) + x) |
 *     | sin(ang)  cos(ang) (-x * sin(ang) - y * cos(ang) + y) |
 *     | 0        0         1                                  |
 *
 * rotate(ang)
 *     | cos(ang) -sin(ang) 0 |
 *     | sin(ang)  cos(ang) 0 |
 *     | 0         0        1 |
 *
 * skewX(ang)
 *     | 1 tan(ang) 0 |
 *     | 0 1        0 |
 *     | 0 0        1 |
 *
 * skewY(ang)
 *     | 1        0 0 |
 *     | tan(ang) 1 0 |
 *     | 0        0 1 |
 *
 *
 */
svgtiny_code
svgtiny_parse_transform(const char *text,
			size_t textlen,
			struct svgtiny_transformation_matrix *tm)
{
	const char *cursor = text; /* text cursor */
	const char *textend = text + textlen;
	enum transform_type transform = TRANSFORM_UNK;
	/* mapping of maimum number of parameters for each transform */
	const int param_max[]={0,6,2,2,3,1,1};
	const char *paramend;
	int paramc;
	float paramv[6];
	svgtiny_code err;

	/* advance cursor past optional whitespace */
	advance_whitespace(&cursor, textend);

	/* zero or more transform followed by whitespace or comma */
	while (cursor < textend) {
		err = parse_transform_function(&cursor, textend, &transform);
		if (err != svgtiny_OK) {
			/* invalid transform */
			goto transform_parse_complete;
		}

		/* advance cursor past optional whitespace */
		advance_whitespace(&cursor, textend);

		/* open parentheses */
		if (*cursor != 0x28 /* ( */) {
			/* invalid syntax */
			goto transform_parse_complete;
		}
		cursor++;

		paramc=param_max[transform];
		err = parse_transform_parameters(&cursor, textend, &paramc, paramv);
		if (err != svgtiny_OK) {
			/* invalid parameters */
			goto transform_parse_complete;
		}
		paramend = cursor;

		/* have transform type and at least one parameter */

		/* apply transform */
		err = apply_transform(transform, paramc, paramv, tm);
		if (err != svgtiny_OK) {
			/* transform failed */
			goto transform_parse_complete;
		}

		/* advance cursor past whitespace (or comma) */
		advance_comma_whitespace(&cursor, textend);
		if (cursor == paramend) {
			/* no comma or whitespace between transforms */
			goto transform_parse_complete;
		}
	}

transform_parse_complete:
	return svgtiny_OK;
}


/**
 * Parse a color.
 *
 * https://www.w3.org/TR/SVG11/types.html#DataTypeColor
 * https://www.w3.org/TR/css-color-5/#typedef-color
 *
 * <color> = <color-base> | currentColor | <system-color>
 * <color-base> = <hex-color> | <color-function> | <named-color> | transparent
 * <color-function> = <rgb()> | <rgba()> | <hsl()> | <hsla()> | <hwb()> |
 *                    <lab()> | <lch()> | <oklab()> | <oklch()> | <color()>
 *
 * \todo this does not cope with currentColor or transparent and supports only
 *       the rgb color function.
 */
svgtiny_code
svgtiny_parse_color(const char *text, size_t textlen, svgtiny_colour *c)
{
	const struct svgtiny_named_color *named_color;
	const char *cursor = text; /* cursor */
	const char *textend = text + textlen;
	svgtiny_code res;

	advance_whitespace(&cursor, textend);

	/* attempt to parse element as a hex color */
	res = parse_hex_color(&cursor, textend, c);
	if (res == svgtiny_OK) {
		return res;
	}

	/* attempt to parse element as a color function */
	res = parse_color_function(&cursor, textend, c);
	if (res == svgtiny_OK) {
		return res;
	}

	named_color = svgtiny_color_lookup(cursor, textend - cursor);
	if (named_color) {
		*c = named_color->color;
		return svgtiny_OK;
	}

	/* did not parse as a color */
	*c = svgtiny_RGB(0, 0, 0);
	return svgtiny_SVG_ERROR;
}

/**
 * parse a viewbox attribute
 *
 * https://www.w3.org/TR/SVG11/coords.html#ViewBoxAttribute
 * https://www.w3.org/TR/SVG2/coords.html#ViewBoxAttribute
 *
 * <min-x>,? <min-y>,? <width>,? <height>
 */
svgtiny_code
svgtiny_parse_viewbox(const char *text,
		      size_t textlen,
		      float viewport_width,
		      float viewport_height,
		      struct svgtiny_transformation_matrix *tm)
{
	const char *cursor = text; /* text cursor */
	const char *textend = text + textlen;
	const char *paramend;
	float paramv[4];
	int paramidx = 0;
	svgtiny_code res;

	/* advance cursor past optional whitespace */
	advance_whitespace(&cursor, textend);

	for (paramidx = 0; paramidx < 3; paramidx++) {
		paramend = textend;
		res = svgtiny_parse_number(cursor, &paramend, &paramv[paramidx]);
		if (res != svgtiny_OK) {
			/* failed to parse number */
			return res;
		}
		cursor = paramend;
		advance_comma_whitespace(&cursor, textend);
	}
	paramend = textend;
	res = svgtiny_parse_number(cursor, &paramend, &paramv[paramidx]);
	if (res != svgtiny_OK) {
		/* failed to parse number */
		return res;
	}
	cursor = paramend;
	advance_whitespace(&cursor, textend);

	if (cursor != textend) {
		/* syntax error */
		return svgtiny_SVG_ERROR;
	}

	tm->a = (float)viewport_width / paramv[2];
	tm->d = (float)viewport_height / paramv[3];
	tm->e += -paramv[0] * tm->a;
	tm->f += -paramv[1] * tm->d;

	return svgtiny_OK;
}


/**
 * parse an inline style
 */
svgtiny_code
svgtiny_parse_inline_style(dom_element *node,
			   struct svgtiny_parse_state *state,
			   struct svgtiny_parse_internal_operation *ops)
{
	const char *cursor; /* text cursor */
	const char *textend;
	const char *declaration_start;
	dom_string *attr;
	dom_exception exc;

	/* style attribute */
	exc = dom_element_get_attribute(node, state->interned_style, &attr);
	if (exc != DOM_NO_ERR) {
		return svgtiny_LIBDOM_ERROR;
	}
	if (attr == NULL) {
		/* no style attribute */
		return svgtiny_OK;
	}
	cursor = dom_string_data(attr);
	textend = cursor + dom_string_byte_length(attr);

	while (cursor < textend) {
		advance_whitespace(&cursor, textend);
		declaration_start = cursor;
		while (cursor < textend) {
			if ((*cursor == ';') &&
			    (*(cursor - 1) != '\\')) {
				break;
			}
			cursor++;
		}
		parse_declaration(declaration_start, cursor, state, ops);
		cursor++; /* skip semicolon */
	}
	dom_string_unref(attr);
	return svgtiny_OK;
}


/**
 * parse attributes controled by operation table
 */
svgtiny_code
svgtiny_parse_attributes(dom_element *node,
			 struct svgtiny_parse_state *state,
			 struct svgtiny_parse_internal_operation *styleops)
{
	struct svgtiny_parse_internal_operation *styleop;
	dom_string *attr;
	dom_exception exc;

	for (styleop = styleops; styleop->key != NULL; styleop++) {
		exc = dom_element_get_attribute(node, styleop->key, &attr);
		if (exc != DOM_NO_ERR) {
			return svgtiny_LIBDOM_ERROR;
		}
		if (attr != NULL) {
			dispatch_op(dom_string_data(attr),
				    dom_string_byte_length(attr),
				    state,
				    styleop);
			dom_string_unref(attr);
		}
	}
	return svgtiny_OK;
}


/**
 * parses href attribute contents as a url fragment and finds matching element
 *
 * \param node The node on which to examine teh href attribute for a url
 * \param state The parse state
 * \param element result element pointer or NULL if no matching element
 * \return svgtiny_OK and element updated else error code
 */
svgtiny_code
svgtiny_parse_element_from_href(dom_element *node,
				struct svgtiny_parse_state *state,
				dom_element **element)
{
	dom_exception exc;
	dom_string *attr;
	svgtiny_code res;
	const char *url;

	/* attempt to get href in default namespace */
	exc = dom_element_get_attribute(node, state->interned_href, &attr);
	if (exc != DOM_NO_ERR) {
		return svgtiny_LIBDOM_ERROR;
	}

	if (attr == NULL) {
		dom_string *xmlns_xlink;
		exc = dom_string_create_interned((const uint8_t *)XLINK_NS,
						 strlen(XLINK_NS),
						 &xmlns_xlink);
		if (exc != DOM_NO_ERR || xmlns_xlink == NULL) {
			return svgtiny_LIBDOM_ERROR;
		}

		/* attempt to get href attribute in xlink namespace */
		exc = dom_element_get_attribute_ns(node,
						   xmlns_xlink,
						   state->interned_href,
						   &attr);
		dom_string_unref(xmlns_xlink);
		if (exc != DOM_NO_ERR) {
			return svgtiny_LIBDOM_ERROR;
		}
		if (attr == NULL) {
			/* no href attribute */
			*element = NULL;
			return svgtiny_OK;
		}
	}

	url = dom_string_data(attr);
	res = element_from_url(&url,
			       dom_string_byte_length(attr),
			       state,
			       element);

	dom_string_unref(attr);
	return res;
}
